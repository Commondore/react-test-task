import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      '@assets': path.resolve(__dirname, './src/assets'),
      '@components': path.resolve(__dirname, './src/components'),
      '@hooks': path.resolve(__dirname, './src/hooks'),
      '@store': path.resolve(__dirname, './src/store'),
      '@interfaces': path.resolve(__dirname, './src/interfaces'),
      '@icons': path.resolve(__dirname, './src/components/icons'),
      '@shared': path.resolve(__dirname, './src/components/shared'),
    },
  },
  plugins: [react()],
});
