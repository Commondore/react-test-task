import styles from './styles.module.css';
const Loading = () => {
  return (
    <div className={styles.spinner}>
      {Array.from(Array(4), (_, key) => (
        <div key={key} />
      ))}
    </div>
  );
};

export default Loading;
