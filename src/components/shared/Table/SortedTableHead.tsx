import cn from 'clsx';
import { ArrowIcon } from '@components/icons';
import styles from './styles.module.css';

interface SortedTableHeadProps extends React.ComponentPropsWithoutRef<'th'> {
  children: React.ReactNode;
  onSort: () => void;
  active: boolean;
}
export const SortedTableHead = ({
  children,
  onSort,
  active,
  ...props
}: SortedTableHeadProps) => {
  return (
    <th {...props}>
      <div
        className={cn(styles.thInner, active && styles.sortActive)}
        onClick={onSort}
      >
        <span>{children}</span>
        <ArrowIcon />
      </div>
    </th>
  );
};
