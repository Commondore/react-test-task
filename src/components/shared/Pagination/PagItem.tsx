import cn from 'clsx';
import styles from './styles.module.css';
interface PaginationItemProps {
  page: number;
  isDisabled: boolean;
  onPageChanged: (page: number) => void;
}

const PagItem = ({ page, isDisabled, onPageChanged }: PaginationItemProps) => {
  return (
    <li className={styles.paginationItem}>
      <button
        type='button'
        disabled={isDisabled}
        className={cn(
          styles.paginationBtn,
          isDisabled && styles.paginationBtnActive,
        )}
        aria-label={`Go to page number ${page}`}
        onClick={() => onPageChanged(page)}
      >
        {page}
      </button>
    </li>
  );
};

export default PagItem;
