import { useState, useEffect, memo } from 'react';
import cn from 'clsx';
import PagItem from '@shared/Pagination/PagItem.tsx';

import styles from './styles.module.css';

interface PaginationProps {
  maxVisibleButtons?: number;
  totalPages: number;
  currentPage: number;
  onPageChanged: (page: number) => void;
}

const Pagination = ({
  maxVisibleButtons = 5,
  totalPages,
  currentPage,
  onPageChanged,
}: PaginationProps) => {
  const [startPage, setStartPage] = useState(
    getStartPage(currentPage, totalPages, maxVisibleButtons),
  );
  const [endPage, setEndPage] = useState(
    getEndPage(currentPage, totalPages, maxVisibleButtons),
  );

  useEffect(() => {
    setStartPage(getStartPage(currentPage, totalPages, maxVisibleButtons));
    setEndPage(getEndPage(currentPage, totalPages, maxVisibleButtons));
  }, [currentPage, totalPages, maxVisibleButtons]);

  function getStartPage(
    currentPage: number,
    totalPages: number,
    maxVisibleButtons: number,
  ): number {
    if (totalPages <= maxVisibleButtons) {
      return 1;
    }

    const halfMaxButtons = Math.floor(maxVisibleButtons / 2);
    if (currentPage <= halfMaxButtons) {
      return 1;
    } else if (currentPage + halfMaxButtons >= totalPages) {
      return totalPages - maxVisibleButtons + 1;
    } else {
      return currentPage - halfMaxButtons;
    }
  }

  function getEndPage(
    currentPage: number,
    totalPages: number,
    maxVisibleButtons: number,
  ): number {
    if (totalPages <= maxVisibleButtons) {
      return totalPages;
    }

    const halfMaxButtons = Math.floor(maxVisibleButtons / 2);
    if (currentPage <= halfMaxButtons) {
      return Math.min(totalPages, maxVisibleButtons);
    } else if (currentPage + halfMaxButtons >= totalPages) {
      return totalPages;
    } else {
      return currentPage + halfMaxButtons;
    }
  }

  function onClickPreviousPage() {
    onPageChanged(currentPage - 1);
  }

  function onClickPage(page: number) {
    onPageChanged(page);
  }

  function onClickNextPage() {
    onPageChanged(currentPage + 1);
  }

  return (
    <ul className={styles.pagination}>
      <li className={cn(styles.paginationItem, styles.paginationPrev)}>
        <button
          type='button'
          disabled={currentPage === 1}
          aria-label='Go to previous page'
          onClick={onClickPreviousPage}
        >
          Назад
        </button>
      </li>
      {Array.from(
        { length: endPage - startPage + 1 },
        (_, index) => startPage + index,
      ).map((page) => (
        <PagItem
          key={page}
          page={page}
          isDisabled={currentPage === page}
          onPageChanged={onClickPage}
        />
      ))}
      <li className={cn(styles.paginationItem, styles.paginationNext)}>
        <button
          type='button'
          disabled={currentPage === totalPages}
          aria-label='Go to next page'
          onClick={onClickNextPage}
        >
          Вперед
        </button>
      </li>
    </ul>
  );
};

export default memo(Pagination);
