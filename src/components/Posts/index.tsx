import { Post } from '@interfaces/post.ts';
import { SortedTableHead } from '@shared/Table';
import PostItem from '@components/Posts/PostItem.tsx';
import Loading from '@shared/Loading';

interface PostsProps {
  posts: Post[] | undefined;
  onSort: (type: string) => void;
  selected: string;
  loading: boolean;
}

const SORT_TITLES: {
  [key: string]: string;
} = {
  id: 'ID',
  title: 'Заголовок',
  body: 'Описание',
};
const Posts = ({ posts, onSort, selected, loading }: PostsProps) => {
  return (
    <table className='table table-bordered'>
      <thead className='table-dark'>
        <tr>
          {Object.keys(SORT_TITLES).map((item) => (
            <SortedTableHead
              onSort={() => onSort(item)}
              key={item}
              active={selected === item}
            >
              {SORT_TITLES[item]}
            </SortedTableHead>
          ))}
        </tr>
      </thead>

      <tbody>
        {loading ? (
          <tr>
            <td colSpan={3} style={{ textAlign: 'center' }}>
              <Loading />
            </td>
          </tr>
        ) : (
          posts && posts.map((post) => <PostItem key={post.id} post={post} />)
        )}
      </tbody>
    </table>
  );
};

export default Posts;
