import { Post } from '@interfaces/post.ts';

interface PostItemProps {
  post: Post;
}
const PostItem = ({ post }: PostItemProps) => {
  const { id, title, body } = post;
  return (
    <tr>
      <td style={{ textAlign: 'center' }}>{id}</td>
      <td>{title}</td>
      <td>{body}</td>
    </tr>
  );
};

export default PostItem;
