import { SearchIcon } from '@components/icons';
import styles from './styles.module.css';
import { useState, memo } from 'react';

interface SearchProps {
  onSearch: (query: string) => void;
}
const Search = ({ onSearch }: SearchProps) => {
  const [value, setValue] = useState('');
  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onSearch(value);
  };

  const onReset = () => {
    setValue('');
    onSearch('');
  };

  return (
    <div className={styles.searchWrap}>
      <form onSubmit={onSubmit} className={styles.searchForm}>
        <div className={styles.search}>
          <input
            className={styles.input}
            value={value}
            onChange={(e) => setValue(e.target.value)}
            type='text'
            placeholder='Поиск'
          />
          <button className={styles.searchButton}>
            <SearchIcon className={styles.icon} />
          </button>
        </div>
      </form>
      {!!value && (
        <button className={styles.resetBtn} onClick={onReset}>
          Обнулить результат поиска
        </button>
      )}
    </div>
  );
};

export default memo(Search);
