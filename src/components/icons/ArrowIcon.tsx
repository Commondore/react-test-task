export const ArrowIcon = ({
  width = 12,
  height = 6,
  viewBox = '0 0 16 10',
  color = 'white',
  ...props
}) => (
  <svg
    width={width}
    height={height}
    viewBox={viewBox}
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...props}
  >
    <path
      d='M1 1L8 8L15 1'
      stroke={color}
      strokeWidth='2'
      strokeLinecap='round'
    />
  </svg>
);
