export interface Post {
  body: string;
  id: number;
  title: string;
  userId: number;
}

export enum SORT_TYPE {
  ASC = 'asc',
  DESC = 'desc',
}

export interface ParamsQuery {
  _limit: number;
  q?: string;
  _sort: string;
  _order: SORT_TYPE;
  _page: number;
}

export interface ApiResponse {
  posts: Post[];
  totalCount: number;
}
