import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { ApiResponse, ParamsQuery, Post } from '@interfaces/post.ts';
import * as qs from 'qs';

const postsApi = createApi({
  reducerPath: 'postsApi',
  tagTypes: ['posts'],
  baseQuery: fetchBaseQuery({ baseUrl: import.meta.env.VITE_BASE_API_URL }),
  endpoints: (builder) => ({
    getPosts: builder.query<ApiResponse, ParamsQuery>({
      query: (params: ParamsQuery) => `posts?${qs.stringify(params)}`,

      transformResponse(posts: Post[], meta): ApiResponse {
        return {
          posts: posts || [],
          totalCount:
            meta && meta.response
              ? Number(meta.response.headers.get('X-Total-Count'))
              : 0,
        };
      },

      providesTags: () => [{ type: 'posts', id: 'LIST' }],
    }),
  }),
});

export default postsApi;
export const { useGetPostsQuery } = postsApi;
