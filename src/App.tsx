import { useGetPostsQuery } from '@store/posts/posts.api.ts';
import * as qs from 'qs';
import Posts from '@components/Posts';
import Search from '@components/Search';
import { useCallback, useEffect, useState } from 'react';
import { ParamsQuery, SORT_TYPE } from '@interfaces/post.ts';
import Pagination from '@shared/Pagination';

function App() {
  const [searchParams, setSearchParams] = useState<ParamsQuery>({
    _limit: 10,
    _sort: 'id',
    _order: SORT_TYPE.ASC,
    _page: 1,
  });
  const { data, isFetching } = useGetPostsQuery(searchParams);

  useEffect(() => {
    const url = new URL(window.location.href);
    const params = new URLSearchParams(url.search).toString();
    if (params) {
      const parse = qs.parse(params);
      setSearchParams((prev) => ({ ...prev, ...parse }));
    }
  }, []);

  useEffect(() => {
    window.history.replaceState(
      {},
      '',
      `?${qs.stringify({
        ...searchParams,
      })}`,
    );
  }, [searchParams]);

  const onSearchHandler = useCallback((query: string) => {
    setSearchParams((prev) => ({
      ...prev,
      q: query,
      _page: 1,
    }));
  }, []);

  const onTableSorted = useCallback((type: string) => {
    setSearchParams((prev) => ({
      ...prev,
      _sort: type,
    }));
  }, []);

  const onPaginate = useCallback((page: number) => {
    setSearchParams((prev) => ({
      ...prev,
      _page: page,
    }));
  }, []);

  const totalPages = data
    ? Math.ceil(data.totalCount / searchParams._limit)
    : 0;

  return (
    <div className='wrap'>
      <div className='container'>
        <Search onSearch={onSearchHandler} />

        <Posts
          posts={data?.posts}
          onSort={onTableSorted}
          selected={searchParams._sort}
          loading={isFetching}
        />
        <Pagination
          totalPages={totalPages}
          currentPage={+searchParams._page}
          maxVisibleButtons={5}
          onPageChanged={onPaginate}
        />
      </div>
    </div>
  );
}

export default App;
